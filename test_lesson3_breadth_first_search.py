#!/usr/bin/env python
# -*- coding: utf8 -*-

"""Describe what this package does."""

__author__ = 'Hwasung Lee'


import unittest
import lesson3_breadth_first_search


class TestBreadthFirstSearch(unittest.TestCase):
  """Standard test class inheriting from unittest.TestCase"""

  def setUp(self):
    """Sets up a simple graph."""
    self.graph = {}
    link_edges = [(1, 2), (2, 3), (3, 4), (2, 4)]
    for node1, node2 in link_edges:
      self.graph = lesson3_breadth_first_search.MakeLink(
          self.graph, node1, node2)

  def testPath(self):
    """Tests whether we find the correct path."""
    expected = [1, 2, 4]
    result = lesson3_breadth_first_search.Path(self.graph, 1, 4)
    self.assertEqual(expected, result)

  def testPathFunctional(self):
    """Tests whether we find the correct path."""
    expected = [1, 2, 4]
    result = lesson3_breadth_first_search.PathFunctional(self.graph, 1, 4)
    self.assertEqual(expected, result)



if __name__ == '__main__':
  unittest.main()

