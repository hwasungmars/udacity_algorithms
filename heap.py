#!/usr/bin/env python3
# -*- coding: utf8 -*-

"""Heap implmentation."""

__author__ = 'Hwasung Lee'


def LeftChildIndex(i):
  """Given an index i find the left child index and return.

  This doesn't guarantee that the node actually has a left child.
  """
  return 2*i + 1


def RightChildIndex(i):
  """Given an index i find the right child index and return.

  This doesn't guarantee that the node actually has a right child.
  """
  return 2*i + 2


def ParentIndex(i):
  """Given an index i return the index of its parent."""
  return (i-1)//2


def IsLeaf(L, i):
  """Given an index i return True if it is a leaf, False otherwise."""
  return LeftChildIndex(i) >= len(L)


def HasOneChild(L, i):
  """Given an index i return True if it has only one child, False otherwise."""
  return RightChildIndex(i) == len(L)


def UpHeapify(L, i):
  """Given an index i, up heapify and return the final position of i."""
  if i == 0:
    return i

  if L[i] < L[ParentIndex(i)]:
    L[ParentIndex(i)], L[i] = L[i], L[ParentIndex(i)]
    return UpHeapify(L, ParentIndex(i))
  else:
    return i


def DownHeapify(L, i):
  """Given an index i, down heapify an return the final position of i."""
  if IsLeaf(L, i):
    return i

  if HasOneChild(L, i):
    if L[LeftChildIndex(i)] < L[i]:
      L[LeftChildIndex(i)], L[i] = L[i], L[LeftChildIndex(i)]
      return DownHeapify(L, LeftChildIndex(i))
    else:
      return i

  # We are left with a node with two leaves.
  if min(L[LeftChildIndex(i)], L[RightChildIndex(i)]) < L[i]:
    index_smaller = min(LeftChildIndex(i), RightChildIndex(i),
                        key=lambda x: L[x])
    L[index_smaller], L[i] = L[i], L[index_smaller]
    return DownHeapify(L, index_smaller)
  else:
    return i


def ExtractMin(L):
  """Extract minimum of the heap and keep the heap property."""
  minimum = L[0]
  L[0] = L.pop(len(L)-1)
  DownHeapify(L, 0)

  return minimum


def Insert(L, element):
  """Insert a element to the heap and return its index."""
  L.append(element)
  return UpHeapify(L, len(L)-1)


def main():
  """main function which runs when the script is called standalone."""
  pass


if __name__ == '__main__':
  main()

