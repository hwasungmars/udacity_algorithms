Udacity
=======

Problem sheets, answers, codes for Udacity courses.


Installation
------------

The repository should be cloned into a path under Pants to use the deploy
script. The repository ideally should be mounted in:

  commons/src/repositories/udacity_algorithms
