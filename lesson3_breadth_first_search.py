#!/usr/bin/env python3
# -*- coding: utf8 -*-

"""Udacity's breadth search implementation."""

__author__ = 'Hwasung Lee'


import csv


def MakeLink(G, node1, node2):
  """Make a link in the graph between node1 and node2."""
  if node1 not in G:
    G[node1] = {}
  if node2 not in G:
    G[node2] = {}

  G[node1][node2] = 1
  G[node2][node1] = 1

  return G


def ReadGraph(filename):
  """Reads an undirected graph in TSV format.

  Each line is an edge.
  """
  tsv = csv.reader(open(filename), delimiter='\t')
  G = {}
  for node1, node2 in tsv:
    MakeLink(G, node1, node2)

  return G


def Path(G, v1, v2):
  """Breadth first search with open list."""
  distance_from_start = {}
  open_list = [v1]
  distance_from_start[v1] = [v1]
  while len(open_list) > 0:
    current = open_list[0]
    del open_list[0]
    for neighbour in G[current]:
      if neighbour not in distance_from_start:
        distance_from_start[neighbour] = (
            distance_from_start[current] + [neighbour])
        if neighbour == v2:
          return distance_from_start[v2]
        else:
          open_list.append(neighbour)

  return False


def PathFunctional(G, v1, v2):
  """Breadth first search, functional programming version."""
  def _Iterator(open_list, paths):
    """Iterator that goes through the open list and finds the paths."""
    if len(open_list) == 0:
      return paths

    current = open_list[0]
    for neighbour in G[current]:
      if neighbour not in paths:
        paths[neighbour] = paths[current] + [neighbour]
        open_list.append(neighbour)

    return _Iterator(open_list[1:], paths)

  return _Iterator([v1], {v1: [v1]})[v2]


def main():
  """main function which runs when the script is called standalone."""
  marvel_graph = ReadGraph('Marvel_graph.tsv')
  from_node = 'A'
  to_node = 'ZZZAX'
  print(Path(marvel_graph, from_node, to_node))


if __name__ == '__main__':
  main()

