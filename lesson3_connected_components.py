#!/usr/bin/env python3
# -*- coding: utf8 -*-

"""Connected components from lesson 3."""

__author__ = 'Hwasung Lee'


def MakeLink(graph, first_node, second_node):
  """Given a graph graph, make a connection between first and second node."""
  if first_node not in graph:
    graph[first_node] = {}
  if second_node not in graph:
    graph[second_node] = {}

  graph[first_node][second_node] = 1
  graph[second_node][first_node] = 1

  return graph


def ConnectedComponents(graph, node):
  """Takes in a graph and a node and return all the connected nodes."""
  def _ConnectedComponents(node, visited):
    # If the node have no neighbours return the visited graph.
    if not graph[node]:
      return visited
    # If all of the neighbours have been visited return the visited graph.
    elif set(graph[node]) <= set(visited):
      return visited
    else:
      for neighbour in graph[node]:
        if not neighbour in visited:
          visited.append(neighbour)
          return _ConnectedComponents(neighbour, visited)

  return _ConnectedComponents(node, [node])


def Components(graph):
  """Takes a graph and returns the connected components."""
  components = []
  for node in graph:
    components.append(frozenset(ConnectedComponents(graph, node)))

  return set(components)


def main():
  """main function which runs when the script is called standalone."""
  pass


if __name__ == '__main__':
  main()

