#!/usr/bin/env python3
# -*- coding: utf8 -*-

"""Udacity: Algorithms Eulerian tour problem."""

__author__ = 'Hwasung Lee'


# Find Eulerian Tour
#
# Write a function that takes in a graph represented as a list of tuples and
# return a list of nodes that you would follow on an Eulerian Tour.
#
# For example, if the input graph was
#   [(1, 2), (2, 3), (3, 1)]
# A possible Eulerian tour would be [1, 2, 3, 1].


from collections import Counter


def FindDegreeOfNodes(graph):
    """For given graph as a list of tuples, return the degree dictionary."""
    degree = Counter()
    for start, end in graph:
      degree[start] += 1
      degree[end] += 1

    return degree


def IsGraphEulerian(degree):
  """Given a dgree of nodes, determine if a graph is Eulerian.

  If the graph is Eulerian return the beginning and end node as a tuple. If not
  return (None, None). If empty degree is given it raises LookupError.
  """
  if not degree:
    raise LookupError('Given graph is empty.')

  odd_nodes = []
  for node in degree:
    if degree[node] % 2 == 1:
      odd_nodes.append(node)

  if len(odd_nodes) == 2:
    return tuple(odd_nodes)
  elif len(odd_nodes) == 0:
    # If we have no odd nodes, we just two even nodes.
    return tuple(list(degree.keys())[:2])
  else:
    return (None, None)


def find_eulerian_tour(graph):
    # 1. Determine if the graph can be Euler toured.
    # 2. Find first and last node.
    # 3. Start from first node, pick a random edge and use that edge.
    #   a. If the visited node is not a dead end and there are remaining edges,
    #      mark the edge we have used as visited and pop it off the list.
    #   b. If the visited node is a dead end and there are remaining edges,
    #      Recover back and try a different edge to cross.
    #   c. If the visited node is a dead end and there are no remaining edges, we are done.
    return []






def main():
  """main function which runs when the script is called standalone."""
  pass


if __name__ == '__main__':
  main()

