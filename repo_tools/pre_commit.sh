#!/usr/bin/env bash
# -*- coding: utf8 -*-

# Author: Hwasung Lee

# Stash things that are modified but not going to be committed.
git stash -q --keep-index

# Run pre_commit tasks using gradle and save the result.
gradle pre_commit --parallel
RESULT=$?

# Pop the temporary stashed items.
git stash pop -q

if [ ${RESULT} -ne 0 ]; then
  echo 'git pre-commit] Detected error in linting or testing.'
  echo 'git pre-commit] If running "gradle pre_commit" passes,'
  echo 'git pre-commit] most likely you forgot to git add a file.'
  exit ${RESULT}
else
  exit 0
fi
