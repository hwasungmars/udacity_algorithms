#!/usr/bin/env bash
# -*- coding: utf8 -*-

# Author: Hwasung Lee

ln -s -f ../../repo_tools/pre_commit.sh $(git rev-parse --show-toplevel)/.git/hooks/pre-commit
