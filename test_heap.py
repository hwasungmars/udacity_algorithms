#!/usr/bin/env python
# -*- coding: utf8 -*-

"""Tests heap property and code."""

__author__ = 'Hwasung Lee'


import unittest
import heap


class TestHeap(unittest.TestCase):
  """Standard test class inheriting from unittest.TestCase"""

  def testHelperMethods(self):
    """Tests for helper methods."""
    expected = 0
    result = heap.ParentIndex(1)
    self.assertEqual(expected, result)

    result = heap.ParentIndex(2)
    self.assertEqual(expected, result)

    heap_list = [1, 3, 2, 6, 7, 5]
    self.assertFalse(heap.IsLeaf(heap_list, 0))
    self.assertTrue(heap.IsLeaf(heap_list, 3))

    self.assertFalse(heap.HasOneChild(heap_list, 0))
    self.assertFalse(heap.HasOneChild(heap_list, len(heap_list)))
    self.assertTrue(heap.HasOneChild(heap_list, 2))

  def testUpHeapify(self):
    """UpHeapify should push smaller elments up the tree."""
    heap_list = [2, 3, 4, 6, 1, 2]
    up_heapify_index = 0
    expected = 0
    result = heap.UpHeapify(heap_list, up_heapify_index)
    self.assertEqual(expected, result)

    up_heapify_index = 5
    expected = 2
    result = heap.UpHeapify(heap_list, up_heapify_index)
    self.assertEqual(expected, result)

  def testDownHeapify(self):
    """DownHeapify should push smaller elments down the tree."""
    heap_list = [3, 1, 4, 2, 5, 6]
    down_heapify_index = 0
    expected = 3
    result = heap.DownHeapify(heap_list, down_heapify_index)
    self.assertEqual(expected, result)

    heap_list = [3, 1, 4, 2, 5, 6]
    down_heapify_index = len(heap_list)-1
    expected = len(heap_list)-1
    result = heap.DownHeapify(heap_list, down_heapify_index)
    self.assertEqual(expected, result)

    heap_list = [3, 2, 1, 4, 5, 6]
    down_heapify_index = 0
    expected = 2
    result = heap.DownHeapify(heap_list, down_heapify_index)
    self.assertEqual(expected, result)

  def testExtractMin(self):
    """ExtractMin pops the minimum elment and keeps the heap property."""
    heap_list = [1, 2, 3, 4, 5]
    expected_min = 1
    result_min = heap.ExtractMin(heap_list)
    self.assertEqual(expected_min, result_min)

    expected_heap = [2, 4, 3, 5]
    self.assertEqual(expected_heap, heap_list)

  def testInsert(self):
    """Insert should insert an elment to the heap and return the final index."""
    heap_list = [1, 4, 5, 2]
    element = 3
    expected = 1
    result = heap.Insert(heap_list, element)
    self.assertEqual(expected, result)


if __name__ == '__main__':
  unittest.main()

