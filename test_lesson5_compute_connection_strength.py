#!/usr/bin/env python
# -*- coding: utf8 -*-

"""Test for copmpute connection strength."""

__author__ = 'Hwasung Lee'


import unittest
import lesson5_compute_connection_strength


class TestComputeConnectionStrength(unittest.TestCase):
  """Standard test class inheriting from unittest.TestCase"""

  def testCollapseBipartite(self):
    """We should collapse bipartite graph to a normal one."""
    bipartite_graph = {
        'a': {'zabc': 1, 'zac': 1},
        'b': {'zabc': 1},
        'c': {'zabc': 1, 'zac': 1},
        'zabc': {'a': 1, 'b': 1, 'c': 1},
        'zac': {'a': 1, 'c': 1}}
    expected = {
        'a': {'b': 1, 'c': 2},
        'b': {'a': 1, 'c': 1},
        'c': {'a': 2, 'b': 1},
        'zabc': {'zac': 2},
        'zac': {'zabc': 2}}
    result = lesson5_compute_connection_strength.CollapseBipartite(
        bipartite_graph)
    self.assertEqual(expected, result)

  def testFlattenDoubleDict(self):
    """Flatten double dict should return a dict with flattend key value."""
    double_dict = {'a': {'b': 1}}
    expected = {'a + b': 1}
    result = lesson5_compute_connection_strength.FlattenDoubleDict(double_dict)
    self.assertEqual(expected, result)

if __name__ == '__main__':
  unittest.main()

