#!/usr/bin/env python
# -*- coding: utf8 -*-

"""Tests for Udacity Eulerian tour"""

__author__ = 'Hwasung Lee'


import unittest
import problem_set1_eulerian_tour


class TestEulerianTour(unittest.TestCase):
  """Standard test class inheriting from unittest.TestCase"""

  def testfindDegreeOfNodes(self):
    """Tests whether we can find the correct degree of nodes."""
    graph = [(1, 2), (2, 3), (3, 1)]
    expected = {1: 2, 2: 2, 3: 2}
    result = problem_set1_eulerian_tour.FindDegreeOfNodes(graph)
    self.assertEqual(expected, result)

  def testIsGraphEulerianRaises(self):
    """Tests if IsGraphEulerian raises exception."""
    self.assertRaises(LookupError,
                      problem_set1_eulerian_tour.IsGraphEulerian,
                      {})

  def testIsGraphEulerian(self):
    """Test if IsGraphEulerian finds the correct start end nodes."""
    # If the number of odd nodes is not 0 or 2, we need to return (None, None).
    degree = {1: 3, 2: 1, 3: 5}
    expected = (None, None)
    result = problem_set1_eulerian_tour.IsGraphEulerian(degree)
    self.assertEqual(expected, result)

    # If the number of odd nodes is 0, we just pick two elements from the graph
    # as start and end node.
    degree = {1: 2, 2: 2, 3: 2}
    expected = (1, 2)
    result = problem_set1_eulerian_tour.IsGraphEulerian(degree)
    self.assertEqual(expected, result)

    # When the graph has two odd nodes, they are start and end by default.
    graph = [(1, 2)]
    expected = (1, 2)
    degree = problem_set1_eulerian_tour.FindDegreeOfNodes(graph)
    result = problem_set1_eulerian_tour.IsGraphEulerian(degree)
    self.assertEqual(expected, result)


if __name__ == '__main__':
  unittest.main()

