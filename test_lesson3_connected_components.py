#!/usr/bin/env python
# -*- coding: utf8 -*-

"""Describe what this package does."""

__author__ = 'Hwasung Lee'


import unittest
import lesson3_connected_components


class TestConnectedComponents(unittest.TestCase):
  """Standard test class inheriting from unittest.TestCase"""

  def testConnectedComponents(self):
    """Check whether connected components are found correctly."""
    graph = {'a': {'b': 1}, 'b': {'a': 1}, 'c': {'d': 1}, 'd': {'c': 1}}
    expected = ['a', 'b']
    result = lesson3_connected_components.ConnectedComponents(graph, 'a')
    self.assertEqual(expected, result)

  def testComponents(self):
    """Check whether connected components are found correctly."""
    graph = {'a': {'b': 1}, 'b': {'a': 1}, 'c': {'d': 1}, 'd': {'c': 1}}
    expected = {frozenset(['a', 'b']), frozenset(['c', 'd'])}
    result = lesson3_connected_components.Components(graph)
    self.assertEqual(expected, result)



if __name__ == '__main__':
  unittest.main()

