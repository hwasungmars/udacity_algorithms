import csv
import operator


def make_link(G, node1, node2):
  if node1 not in G:
    G[node1] = {}
  if node2 not in G:
    G[node2] = {}

  if node2 not in G[node1]:
    (G[node1])[node2] = 1
  else:
    (G[node1])[node2] += 1

  if node1 not in G[node2]:
    (G[node2])[node1] = 1
  else:
    (G[node2])[node1] += 1

  return G


def read_graph(filename):
  # Read an undirected graph in CSV format. Each line is an edge
  tsv = csv.reader(open(filename), delimiter='\t')
  G = {}
  for (node1, node2) in tsv:
    make_link(G, node1, node2)
  return G


def CollapseBipartite(bipartite_graph):
  """Takes ina bipartite graph and collapses one connection bridge.

  For example:
    G = {'a': {0.1: 1}, 'b': {0.1: 1}, 0.1: {'a': 1, 'b': 1}}
        => {'a': {'b': 1}, 'b': {'a': 1}}
  """
  collapsed = {}
  for node in bipartite_graph:
    if not node in collapsed:
      collapsed[node] = {}

    for bridge in bipartite_graph[node]:
      for end_node in bipartite_graph[bridge]:
        # We skip circular loops.
        if node == end_node:
          continue

        if end_node not in collapsed[node]:
          collapsed[node][end_node] = 1
        else:
          collapsed[node][end_node] += 1

  return collapsed


def FlattenDoubleDict(double_dict):
  """Takes a doubly nested dict, and flattens it."""
  flattened = {}
  for first in double_dict:
    for second in double_dict[first]:
      flattened_key = '%s + %s' % (first, second)
      flattened[flattened_key] = double_dict[first][second]

  return flattened


if __name__ == '__main__':
  # Read the marvel comics graph
  marvel_bipartite = read_graph('Marvel_graph.tsv')
  marvel_flattened = FlattenDoubleDict(CollapseBipartite(marvel_bipartite))
  print(max(marvel_flattened.items(), key=lambda x: x[1]))
