import operator

f = open('./yob1995.txt', 'r')

name_popularity = {}
for line in f:
  l = line.strip().split(',')
  if l[1] == 'F':
    name_popularity[l[0]] = int(l[2])

sorted_name_popularity = sorted(name_popularity.items(),
                                key=operator.itemgetter(1))
print(sorted_name_popularity)
