#!/usr/bin/env python3
# -*- coding: utf8 -*-

"""Computes the clustering coefficient for a graph."""

__author__ = 'Hwasung Lee'


def MakeLink(G, x, y):
  """Takes in a graph G (dictionary) and creates links between x, y."""
  if x not in G:
    G[x] = {}
  G[x][y] = 1
  if y not in G:
    G[y] = {}
  G[y][x] = 1

  return G


def ClusteringCoefficient(G, x):
  """Find the clustering coefficient for x in graph G."""
  nb_neighbours = len(G[x].keys())
  if nb_neighbours == 1:
    return 0.0

  connection = 0
  for first_neighbour in G[x]:
    for second_neighbour in G[x]:
      if second_neighbour in G[first_neighbour]:
        connection += 0.5

  return connection*2/(nb_neighbours-1)/nb_neighbours


def main():
  """main function which runs when the script is called standalone."""
  pass


if __name__ == '__main__':
  main()

